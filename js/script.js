const button = $('.scroll-top');
const buttonOpen = $('.hide');
const newsContainer = $('.news-container');

$(window).scroll(() => {
    const windowHeight = $(window).height();
    const scrollHeight = $(document.body.parentElement).scrollTop();
    if (scrollHeight > windowHeight) {
        button.fadeIn(500);
    } else {
        button.fadeOut(500);
    }
});

button.click(() => {
    $(document.body.parentElement).animate({scrollTop: 0}, 600);
});

$(buttonOpen).click(() => {
    if (buttonOpen.text() === 'Open') {
        buttonOpen.text('Close');
    } else if (buttonOpen.text() === 'Close') {
        buttonOpen.text('Open');
    }
    newsContainer.slideToggle(400);
});

$('.nav-menu-item').on('click', 'a', function (event) {
    event.preventDefault();
    let id = $(this).attr('href');
    let anchorHeight = $(id).offset().top;
    $(document.body.parentElement).animate({scrollTop: anchorHeight}, 1500)
});
